let foodService = (function() {
	return new Promise((resolve, reject) => {
		resolve(initializeFoodData());
		console.log(InitializeFoodData());
	});
})();

//--------------INITIALIZE FOOD DATA DETAILS ---------------//

function initializeFoodData() {
	let items = new Array();
	foodData.forEach(element => {
		items.push(
			new FoodItemMenu(
				element.itemTitle,
				element.name,
				element.description,
				element.price,
				element.category,
				element.test
			)
		);
	});
	return items;
}
