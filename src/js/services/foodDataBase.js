let foodData = [
	{
		itemTitle: ITEM_GROUP.STARTER,
		name: 'HOMEMADE NACHOS PLATTER',
		description:
			'This is an item on your menu.Give your item a brief description',
		price: '$9',
		category: FOOD_CATEGORY.NONVEG,
		test: FOOD_TEST.NOSPICY
	},
	{
		itemTitle: ITEM_GROUP.STARTER,
		name: 'MEXICAN GRILLED CORN',
		description:
			'This is an item on your menu.Give your item a brief description',
		price: '$9',
		category: FOOD_CATEGORY.NONVEG,
		test: FOOD_TEST.NOSPICY
	},
	{
		itemTitle: ITEM_GROUP.STARTER,
		name: 'CHICKEN FAJITAS SALAD',
		description:
			'This is an item on your menu.Give your item a brief description',
		price: '$9',
		category: FOOD_CATEGORY.NONVEG,
		test: FOOD_TEST.SPICY
	},
	{
		itemTitle: ITEM_GROUP.STARTER,
		name: 'CLASSIC TOMATO SALSA',
		description:
			'This is an item on your menu.Give your item a brief description',
		price: '$9',
		category: FOOD_CATEGORY.NONVEG,
		test: FOOD_TEST.SPICY
	},
	{
		itemTitle: ITEM_GROUP.STARTER,
		name: 'MINI QUESADILLAS',
		description:
			'This is an item on your menu.Give your item a brief description',
		price: '$9',
		category: FOOD_CATEGORY.NONVEG,
		test: FOOD_TEST.NOSPICY
	},
	{
		itemTitle: ITEM_GROUP.BURRITO,
		name: 'CHILLI BURRITO',
		description:
			'This is an item on your menu.Give your item a brief description',
		price: '$9',
		category: FOOD_CATEGORY.NONVEG,
		test: FOOD_TEST.MORESPICY
	},
	{
		itemTitle: ITEM_GROUP.BURRITO,
		name: 'CHICKEN BURRITO',
		description:
			'This is an item on your menu.Give your item a brief description',
		price: '$9',
		category: FOOD_CATEGORY.NONVEG,
		test: FOOD_TEST.NOSPICY
	},
	{
		itemTitle: ITEM_GROUP.BURRITO,
		name: 'FRIED FISH BURRITO',
		description:
			'This is an item on your menu.Give your item a brief description',
		price: '$9',
		category: FOOD_CATEGORY.NONVEG,
		test: FOOD_TEST.NOSPICY
	},
	{
		itemTitle: ITEM_GROUP.BURRITO,
		name: 'VEGETARIAN BURRITO',
		description:
			'This is an item on your menu.Give your item a brief description',
		price: '$9',
		category: FOOD_CATEGORY.VEG,
		cest: FOOD_TEST.NOSPICY
	},
	{
		itemTitle: ITEM_GROUP.BURRITO,
		name: 'FAJITA BEEF BURRITO',
		description:
			'This is an item on your menu.Give your item a brief description',
		price: '$9',
		category: FOOD_CATEGORY.NONVEG,
		test: FOOD_TEST.NOSPICY
	}
];
