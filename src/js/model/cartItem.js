class CartItem {
	constructor(foodId,foodName,foodDesc,quantity,foodPrice,orderPrice,specialRequest) {
		this.foodId = foodId;
		this.foodName = foodName;
		this.foodDesc = foodDesc;
		this.quantity = quantity;
		this.foodPrice = foodPrice;
		this.orderPrice = orderPrice;
		this.specialRequest = specialRequest;
    };
	}
