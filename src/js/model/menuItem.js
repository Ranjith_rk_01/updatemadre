//*-----------FOOD-ITEM-CONSTANT-DECLARATION----------------- *//

const ITEM_GROUP = {
	STARTER: 'STARTER',
	BURRITO: 'BURRITO'
};
const FOOD_CATEGORY = {
	VEG: 'vegetarian',
	NONVEG: 'non-vegetarian'
};

const FOOD_TEST = {
	NOSPICY: 'no-spicy',
	SPICY: 'spicy',
	MORESPICY: 'extra-spicy'
};

const FOOD_TYPE_DETAIL = {
	STARTER :
	{
		NAME : 'STARTERS',
		DESCRIPTION : 'This is a section of your menu. Give your item a brief description'
	},
	BURRITO :
	{
		NAME :'BURRITOS',
		DESCRIPTION :'This is a section of your menu. Give your item a brief description'
	}
};

//*-----------------------FOOD-ITEM-MENU-STRUCTURE-------------------------*//
class FoodItemMenu {
	constructor(itemTitle, name, description, price, category, test) {
		this.itemTitle = itemTitle;
		this.name = name;
		this.description = description;
		this.price = price;
		this.category = category;
		this.test = test;
	}
}
