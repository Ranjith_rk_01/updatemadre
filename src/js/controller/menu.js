let items = new Array();

foodService.then(result => {
	items = result;
	console.log(items);

	foodItemDom.updateItemUI();
});

let foodItemDom = (function() {
	let menuDOMStrings = {
		starterContainer: '.container-starter',
		burritoContainer: '.container-Burrito'
	};
	return {
		updateItemUI: function() {
			items.forEach((element, index) => {
				let html, newHtml;
				if (element.itemTitle == ITEM_GROUP.STARTER) {
					if (element.test == FOOD_TEST.NOSPICY) {
						html =
							'<div class="col food-1"> <h5 class="food-title">%title%</h5><p class="food-desc food-1-desc">%desc%</p>  <span class="price">%price%</span> </div>';
					} else if (element.test == FOOD_TEST.SPICY) {
						html =
							'<div class="col food-1"> <h5 class="food-title">%title%</h5><p class="food-desc food-1-desc">%desc%</p>  <span class="price">%price%</span><br><br> <i class="ion-bonfire"  title="spicy"></i></div>';
					} else if (element.test == FOOD_TEST.MORESPICY) {
						html =
							'<div class="col food-1"> <h5 class="food-title">%title%</h5><p class="food-desc food-1-desc">%desc%</p>  <span class="price">%price%</span><br><br> <i class="ion-bonfire"  title="Extra-hot"> <i class="ion-bonfire"  title="Extra-hot"> <i class="ion-bonfire"  title="Extra-hot"></i></div>';
					}
					newHtml = html.replace('%title%', element.name);
					newHtml = newHtml.replace('%desc%', element.description);
					newHtml = newHtml.replace('%price%', element.price);
					document
						.querySelector(menuDOMStrings.starterContainer)
						.insertAdjacentHTML('beforeend', newHtml);
				}

				if (element.itemTitle == ITEM_GROUP.BURRITO) {
					if (element.test == FOOD_TEST.NOSPICY) {
						html =
							'<div class="col food-1"> <h5 class="food-title">%title%</h5><p class="food-desc food-1-desc">%desc%</p>  <span class="price">%price%</span> </div>';
					} else if (element.test == FOOD_TEST.SPICY) {
						html =
							'<div class="col food-1"> <h5 class="food-title">%title%</h5><p class="food-desc food-1-desc">%desc%</p>  <span class="price">%price%</span><br><br> <i class="ion-bonfire"  title="spicy"></i></div>';
					} else if (element.test == FOOD_TEST.MORESPICY) {
						html =
							'<div class="col food-1"> <h5 class="food-title">%title%</h5><p class="food-desc food-1-desc">%desc%</p>  <span class="price">%price%</span><br><br> <i class="ion-bonfire"  title="Extra-hot"> <i class="ion-bonfire"  title="Extra-hot"> <i class="ion-bonfire"  title="Extra-hot"></i></div>';
					} else if (element.category == FOOD_CATEGORY.VEG) {
						html =
							'<div class="col food-1"> <h5 class="food-title">%title%</h5><p class="food-desc food-1-desc">%desc%</p>  <span class="price">%price%</span><br><br> <span class="veg" title="vegetarian">VG</span></div>';
					}

					newHtml = html.replace('%title%', element.name);
					newHtml = newHtml.replace('%desc%', element.description);
					newHtml = newHtml.replace('%price%', element.price);
					document
						.querySelector(menuDOMStrings.burritoContainer)
						.insertAdjacentHTML('beforeend', newHtml);
				}
			});
		}
	};
})();
