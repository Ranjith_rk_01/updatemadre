
let orderOnline = (function() {
	let orderOnlineDOMStrings = {
		orderLunchMenu: '.lunch-item',
		orderMenuRotate: '.iconRotate',
		orderMenuIcon: '.ion-chevron-up',
		optionStarter: '.option-Starters',
		optionBurrito: '.option-Burritos'
	};

	return {
		updateLunchMenu: function() {
			let click = 0;
			document
				.querySelector(orderOnlineDOMStrings.orderLunchMenu)
				.addEventListener('click', function() {
					if (click === 0) {
						document.querySelector(
							orderOnlineDOMStrings.orderMenuRotate
						).style.transform = 'rotate(0deg)';
						document.querySelector(
							orderOnlineDOMStrings.orderMenuRotate
						).style.transition = '0.3s ease-in-out';
						document.querySelector(
							orderOnlineDOMStrings.optionStarter
						).style.display = 'block';
						document.querySelector(
							orderOnlineDOMStrings.optionBurrito
						).style.display = 'block';
						click = 1;
					} else if (click === 1) {
						document.querySelector(
							orderOnlineDOMStrings.orderMenuRotate
						).style.transform = 'rotate(180deg)';
						document.querySelector(
							orderOnlineDOMStrings.orderMenuRotate
						).style.transition = '0.3s ease-in-out';
						document.querySelector(
							orderOnlineDOMStrings.optionStarter
						).style.display = 'none';
						document.querySelector(
							orderOnlineDOMStrings.optionBurrito
						).style.display = 'none';
						click = 0;
					}
				});
		}
	};
})();
orderOnline.updateLunchMenu();
