let cartItemDom = (function()
{
    let pos = 0;
    let addCartDomStrings = {
        addCartDefaultItem : '.add-card-shop-details',
        addCartUpdateItem : '.cart-item-container',
        totalItemConatiner :'.order-no-of-items',
        totalPriceContainer :'.cart-subtotal',
        orderNow :'.order',
        viewItem :'.order-now-container',
        orderDetails :'.order-details-container'
    };
    return {
        updateCartItemContainer : () =>
        {
            document
            .querySelector(addCartDomStrings.addCartDefaultItem).style.display = "none";
           
          
        },
        orderDetails : () => {
            var text = document.querySelector(addCartDomStrings.addCartUpdateItem);
           console.log(text);
           return text;

        },
        updateRemoveCartItem :(cartItem,currentItem)=>
        {
            document.querySelector(`.cart-item-list-container-${pos}`).style.display = "none";
            cartItemDom.updateTotalOrder(cartItem);
        },
        updateTotalOrder :(cartItem)=>
        { 
           let totalOrderItems = cartService.getTotalCartItemLength(cartItem);
           let totalPrice = cartService.getTotalCartPrice(cartItem);
           console.log(totalPrice);
           document.querySelector(addCartDomStrings.totalItemConatiner).textContent = `(${totalOrderItems}  items)`;  
           document.querySelector(addCartDomStrings.totalPriceContainer).textContent = `$${totalPrice}`;  
        },
        addCartItemOrder :function (cartItem,currentItem)
        {
            pos += 1;
            cartItem.forEach((element,index)=>
            {
            if(currentItem === index)
            {
            
            let html;
            html =`<div class="cart-item-list-container-${pos}  cart-item-list-container "><span class="cart-item-container__quantity">
            ${cartItem[index].quantity}
          </span><span class="cart-item-container__price-cal">x</span>
          <span class="cart-item-conainer__food-name">
              ${cartItem[index].foodName}
          </span>
          <div class="cart-item-change">
              <span class="cart-item-change-decre"><i class="ion-ios-minus-outline"  onClick="cartService.removeItemSizeFromCart(${cartItem[index].foodId},${cartItem[index].quantity},${cartItem[index].foodPrice},${currentItem},${cartItemDom.updateTotalOrder(cartItem)})"></i></span>
              <span class="cart-item-change-price">$ ${cartItem[index].orderPrice}</span>
              <span class="cart-item-change-incre"><i class="ion-ios-plus-outline" onClick="cartService.addItemSizeFromCart(${cartItem[index].foodId},${cartItem[index].quantity},${cartItem[index].foodPrice},${currentItem},${cartItemDom.updateTotalOrder(cartItem)})"></i></span>    
          </div></div>`;
          document
          .querySelector(addCartDomStrings.addCartUpdateItem)
          .insertAdjacentHTML('beforeend',html);
          cartItemDom.updateTotalOrder(cartItem);
            }
            });
        },
        viewDetails : (cartItem)=>
        {
            console.log(cartItem);
        }
    }
})();
