foodService.then(result => {
	items = result;
	orderMenu.updateMenuItem();
});

let orderMenu = (function() {
	let orderMenuDOMStrings = {
		optionStarter: '.option-Starters',
		optionBurrito: '.option-Burritos',
		foodDetailsStarter: '.container-starter',
		foodDetailsBurrito: '.container-burrito',
		foodMenuTitleContainer: '.menu-title-container'
	};
	return {
		updateMenuItem: () => {
			orderMenu.clearMenuItemFields();
			orderMenu.updateMenuItemToDisplay(
				ITEM_GROUP.STARTER,
				FOOD_TYPE_DETAIL.STARTER
			);
			document
				.querySelector(orderMenuDOMStrings.optionStarter)
				.addEventListener('click', orderMenu.updateMenuItemStarter);

			document
				.querySelector(orderMenuDOMStrings.optionBurrito)
				.addEventListener('click', orderMenu.updateMenuItemBurrito);
		},
		updateMenuItemToDisplay: (itemType, menuType) => {
			let html, newHtml;
			html =
				'<div class="sub-menu row"><h4>%name%</h4><p>%titleDescription%</p><br>	<span class="empty-line-of-Items"></span>	</div>';

			newHtml = html.replace('%name%', menuType.NAME);
			newHtml = newHtml.replace('%titleDescription%', menuType.DESCRIPTION);

			document
				.querySelector(orderMenuDOMStrings.foodMenuTitleContainer)
				.insertAdjacentHTML('beforeend', newHtml);

			//-------------update-menu-food-item-list --------------------//

			if (itemType === ITEM_GROUP.STARTER) {
				items.forEach((element, index) => {
					if (element.itemTitle === ITEM_GROUP.STARTER) {
						html = `<div class="starter-item-${index +
							1} food-Items" onClick="orderPopUp.updatePopUp(${index +
							1})" id ="food-${index +
							1}"><div class="row-Food-Item"><h5 class="food-title
						  food-title-${index + 1}">%name%</h5>	<span class="price  price-${index +
							1}">%price%</span>	</div><div class="food-desc  food-desc-${index +
							1}"><p>%desc%</p></div>	<div class="empty-Food-line"></div>	</div>`;

						newHtml = html.replace('%name%', element.name);
						newHtml = newHtml.replace('%desc%', element.description);
						newHtml = newHtml.replace('%price%', element.price);
						document
							.querySelector(orderMenuDOMStrings.foodDetailsStarter)
							.insertAdjacentHTML('beforeend', newHtml);
					}
				});
			}
			if (itemType === ITEM_GROUP.BURRITO) {
				items.forEach((element, index) => {
					if (element.itemTitle === ITEM_GROUP.BURRITO) {
						html = `<div class="burrito-item-${index +
							1} food-Items"  onClick ="orderPopUp.updatePopUp(${index +
							1})" id ="food-${index +
							1}"><div class="row-Food-Item"><h5 class="food-title
							food-title-${index + 1}">%name%</h5>	<span class="price  price-${index +
							1}">%price%</span>	</div><div class="food-desc  food-desc-${index +
							1}"><p>%desc%</p></div>	<div class="empty-Food-line"></div>	</div>`;
						newHtml = html.replace('%name%', element.name);
						newHtml = newHtml.replace('%desc%', element.description);
						newHtml = newHtml.replace('%price%', element.price);
						document
							.querySelector(orderMenuDOMStrings.foodDetailsBurrito)
							.insertAdjacentHTML('beforeend', newHtml);
					}
				});
			}
		},
		updateMenuItemStarter: () => {
			orderMenu.clearMenuItemFields();
			orderMenu.updateMenuItemToDisplay(
				ITEM_GROUP.STARTER,
				FOOD_TYPE_DETAIL.STARTER
			);
		},
		updateMenuItemBurrito: () => {
			orderMenu.clearMenuItemFields();
			document.querySelector(orderMenuDOMStrings.foodDetailsBurrito);
			orderMenu.updateMenuItemToDisplay(
				ITEM_GROUP.BURRITO,
				FOOD_TYPE_DETAIL.BURRITO
			);
		},
		clearMenuItemFields: () => {
			document.querySelector(orderMenuDOMStrings.foodDetailsStarter).innerHTML =
				'';
			document.querySelector(
				orderMenuDOMStrings.foodMenuTitleContainer
			).innerHTML = '';
			document.querySelector(orderMenuDOMStrings.foodDetailsBurrito).innerHTML =
				'';
		}
	};
})();
