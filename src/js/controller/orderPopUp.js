foodService.then(result => {
	items = result;
	console.log(items);
	orderPopUp.defaultPopUpItem();
});

var orderPopUp = (function() {
	var cartItem = [],value,quantity;
	var id,name,price,description,finalQuantity,orderTotalPrice,foodId = 100,specialRequest = undefined,currentItem = 0;
	let orderPopUpDomStrings = {
		foodItemDetailList: '.food-Items',
		optionStarter: '.option-Starters',
		optionBurrito: '.option-Burritos',
		orderPopUpContainer: '.pop-up-container',
		orderFoodContainer: '.order-body-container',
		closePopUpBox: '.btn-close',
		popUpFoodName: '.food-details-name',
		popUpFoodDesc: '.food-details-description',
		popUpFoodPrice: '.food-details-price',
		decQuantity: '.ion-ios-minus-outline',
		incQuantity: '.ion-ios-plus-outline',
		quantityChange: '.quantity-count',
		addToCartOrder :'.pop-up-add-order-btn',
		updateDefaultQuantity :'.quantity-count'
	};
	return {
		defaultPopUpItem: function() {
			document.querySelector(
				orderPopUpDomStrings.orderPopUpContainer
			).style.display = 'none';
			document.querySelector(orderPopUpDomStrings.updateDefaultQuantity ).textContent ="1";
			document.querySelector(
				orderPopUpDomStrings.orderFoodContainer
			).style.background = 'rgb(97, 0, 56)';
		},
		decreaseQuantity: function() {
			 value = document.querySelector(orderPopUpDomStrings.quantityChange)
				.innerText;
			 quantity = parseInt(value);
			if (quantity >= 2) {
				quantity -= 1;
				console.log(quantity);
			}
			document.querySelector(
				orderPopUpDomStrings.quantityChange
			).textContent = quantity;
			orderPopUp.finalUpdateQuantity();

		},
		increaseQuantity: function() {
			 value = document.querySelector(orderPopUpDomStrings.quantityChange)
				.innerText;
		      quantity = parseInt(value);
			if (quantity !== 0) {
				quantity += 1;
			}
			document.querySelector(
				orderPopUpDomStrings.quantityChange
			).textContent = quantity;
			orderPopUp.finalUpdateQuantity();
		},	
		finalUpdateQuantity:() =>
		{
			value = document.querySelector(orderPopUpDomStrings.quantityChange)
			.innerText;
			 quantity = parseInt(value);
			return  quantity;
			 	
		},
		generatefoodId	:() =>
		{
			if(foodId !== undefined)
			{
				foodId+=11;
			}
			console.log(foodId);
		},
		getCurrentItem :()=>
		{
               return cartItem;
		},
		getCartItemList : ()=>
		{
			cartItemDom.addCartItemOrder(cartItem,currentItem);
			currentItem += 1;
		},
		addToCartOrder: function()
		{		
			orderPopUp.generatefoodId(foodId);
		    finalQuantity = orderPopUp.finalUpdateQuantity();
			orderTotalPrice = cartService.findCurrentOrderPrice(price,finalQuantity);
			cartItem.push(new CartItem(
				foodId,
				name,
				description,
				quantity,
				price,
				orderTotalPrice,
				specialRequest));
				console.log(cartItem);
				cartService.addToCart(cartItem);  
				orderPopUp.getCurrentItem();
				cartItemDom.updateCartItemContainer();
				let cartItemList = cartService.getCartList();
				orderPopUp.getCartItemList();
				orderPopUp.defaultPopUpItem();	
		},
		updatePopUp: function(event) {
			id = event;
			document.getElementById(`food-${id}`).addEventListener('click', () => {
				name = document.querySelector(`.food-title-${id}`).innerText;
				description = document.querySelector(`.food-desc-${id}`).innerText;
				price = document.querySelector(`.price-${id}`).innerText;

				document.querySelector(
					orderPopUpDomStrings.popUpFoodName
				).textContent = name;	
				document.querySelector(
					orderPopUpDomStrings.popUpFoodDesc
				).textContent = description;
				document.querySelector(
					orderPopUpDomStrings.popUpFoodPrice
				).textContent = price;
				price = 9;
				document.querySelector(
					orderPopUpDomStrings.orderFoodContainer
				).style.background = 'rgba(97, 0, 56,0.7)';
				document.querySelector(
					orderPopUpDomStrings.orderPopUpContainer
				).style.display = 'block';

				document
					.querySelector(orderPopUpDomStrings.decQuantity)
					.addEventListener('click', orderPopUp.decreaseQuantity);
				document
					.querySelector(orderPopUpDomStrings.incQuantity)
					.addEventListener('click', orderPopUp.increaseQuantity);
				document
					.querySelector(orderPopUpDomStrings.closePopUpBox)
					.addEventListener('click', orderPopUp.defaultPopUpItem);

					document.querySelector(orderPopUpDomStrings.addToCartOrder).
					addEventListener('click',orderPopUp.addToCartOrder);
			});
		}
	};
})();
